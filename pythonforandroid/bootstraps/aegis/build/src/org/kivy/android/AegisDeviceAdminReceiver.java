/*
 * Copyright (c) 2017, Fitec Research Ltd. (http://www.fitec.co)
 * All Rights Reserved.
 */

package org.kivy.android;

import static android.content.pm.PackageManager.COMPONENT_ENABLED_STATE_DISABLED;

import android.app.admin.DeviceAdminReceiver;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.PersistableBundle;
import android.util.Log;

/**
 * This is the component that is responsible for actual device administration.
 * It becomes the receiver when a policy is applied. It is important that we
 * subclass DeviceAdminReceiver class here and to implement its only required
 * method onEnabled().
 */
public class AegisDeviceAdminReceiver extends DeviceAdminReceiver {

    private static final String TAG = DeviceAdminReceiver.class.getName();
    public static final String DISALLOW_SAFE_BOOT = "no_safe_boot";

    /**
     * Called when this application is approved to be a device administrator.
     */
    @Override
    public void onEnabled(final Context context, Intent intent) {
        super.onEnabled(context, intent);
    }

    /**
     * Called when this application is no longer the device administrator.
     */
    @Override
    public void onDisabled(Context context, Intent intent) {
        super.onDisabled(context, intent);
    }

    @Override
    public void onPasswordChanged(Context context, Intent intent) {
        super.onPasswordChanged(context, intent);
        Log.d(TAG, "onPasswordChanged.");
    }

    @Override
    public void onPasswordFailed(Context context, Intent intent) {
        super.onPasswordFailed(context, intent);
        Log.d(TAG, "onPasswordFailed.");
    }

    @Override
    public void onPasswordSucceeded(Context context, Intent intent) {
        super.onPasswordSucceeded(context, intent);
        Log.d(TAG, "onPasswordSucceeded.");
    }

    @Override
    public void onProfileProvisioningComplete(Context context, Intent intent) {
        // Disable Wizard
        context.getPackageManager().setComponentEnabledSetting(
                new ComponentName("co.fitec.iot.agent",
                    "co.fitec.iot.agent.activities.InitializationActivity"),
                COMPONENT_ENABLED_STATE_DISABLED, 0 /*DONT_KILL_APP*/);

        PersistableBundle persistableBundle = intent.getParcelableExtra(
                DevicePolicyManager.EXTRA_PROVISIONING_ADMIN_EXTRAS_BUNDLE);
        if (persistableBundle != null) {
            String server = (String) persistableBundle.get("android.app.extra.server");
            String token = (String) persistableBundle.get("android.app.extra.token");
            String auth = (String) persistableBundle.get("android.app.extra.auth");
            if (server != null && !server.isEmpty() &&
                    token != null && !token.isEmpty() &&
                    auth != null && !auth.isEmpty()) {
                SharedPreferences pref = context.getSharedPreferences(
                        "aegis", Context.MODE_PRIVATE);
                pref.edit().putString("provision_server", server).apply();
                pref.edit().putString("provision_token", token).apply();
                pref.edit().putString("provision_auth", auth).apply();
            }
        }
    }
}
