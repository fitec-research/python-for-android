/*
 * Copyright (c) 2017, Fitec Research Ltd. (http://www.fitec.co)
 * All Rights Reserved.
 */

package org.kivy.android;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

public class AegisNetworkConnectedReceiver extends BroadcastReceiver {
	private static final String TAG = AegisNetworkConnectedReceiver.class.getSimpleName();

    @Override
    public void onReceive(final Context context, final Intent intent) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(
                Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        if (activeNetwork != null && activeNetwork.isConnected()) {
            Log.i(TAG, "CCCCCCCCCCCCCCCCCCCCCCCCCCC CONNECTED CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC");
            Log.i(TAG, "SSSSSSSSSSSSSSSSSSStart SYSTEM APP SYSTEM APP SYSTEM APP");
            Utils.callSystemAppInit(context);
        }
    }
}
