/*
 * Copyright (c) 2017, Fitec Research Ltd. (http://www.fitec.co)
 * All Rights Reserved.
 */

package org.kivy.android;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.util.Log;

import java.util.List;

public class Utils {

	public static void callSystemAppInit(Context context) {
        Intent intent =  new Intent("co.fitec.iot.system.service.START_SERVICE");
        Intent explicitIntent = createExplicitFromImplicitIntent(context, intent);
        if (explicitIntent != null) {
            intent = explicitIntent;
        }
        context.startService(intent);
	}

	public static void callSystemApp(Context context, String operation,
            String command, String appUri) {
        Intent intent =  new Intent("co.fitec.iot.system.service.START_SERVICE");
        Intent explicitIntent = createExplicitFromImplicitIntent(context, intent);
        if (explicitIntent != null) {
            intent = explicitIntent;
        }
        intent.putExtra("operation", operation);
        intent.setPackage("co.fitec.aegis.launcher");

        if (appUri != null) {
            intent.putExtra("appUri", appUri);
        }

        if (command != null) {
            if ("UPGRADE_FIRMWARE".equals(operation)) {
                /* Do nothing */
            }
            intent.putExtra("command", command);
        }
        context.startService(intent);
    }

	public static Intent createExplicitFromImplicitIntent(Context context, Intent implicitIntent) {
		//Retrieve all services that can match the given intent
		PackageManager pm = context.getPackageManager();
		List<ResolveInfo> resolveInfo = pm.queryIntentServices(implicitIntent, 0);

		//Make sure only one match was found
		if (resolveInfo == null || resolveInfo.size() != 1) {
            Log.i("TTTTT", "NNNNNNNNNNNNNNNNNNNNNNNNNNNNOTHING MATCH");
			return null;
		}

		//Get component info and create ComponentName
		ResolveInfo serviceInfo = resolveInfo.get(0);
		String packageName = serviceInfo.serviceInfo.packageName;
        Log.i("TTTTT", "NNNNNNNNNNNNNNNNNNNNNNNNNNN PACKAGE NAME: " + packageName);
		String className = serviceInfo.serviceInfo.name;
        Log.i("TTTTT", "NNNNNNNNNNNNNNNNNNNNNNNNNNN CLASS NAME: " + className);
		ComponentName component = new ComponentName(packageName, className);

		//Create a new intent. Use the old one for extras and such reuse
		Intent explicitIntent = new Intent(implicitIntent);

		//Set the component to be explicit
		explicitIntent.setComponent(component);

		return explicitIntent;
	}

}
